.. isisysvic3daccess documentation master file, created by
   sphinx-quickstart on Fri Sep 25 10:54:55 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

======================================
Welcome to trashpanda's documentation!
======================================

**trashpanda** is a collection of helper methods composing tasks around
pandas.DataFrames and Series. Either they are to specific (or I didn't found
them).

.. image:: ../trashpanda-icon.svg
   :height: 196px
   :width: 196px
   :alt: A trash panda.
   :align: center

Installation
============

Install the latest release from pip.

.. code-block:: shell

   $ pip install trashpanda

.. toctree::
   :maxdepth: 3

   api_reference/index

Indices and tables
==================

* :ref:`genindex`

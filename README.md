# trashpanda
[![Coverage Status](https://coveralls.io/repos/gitlab/david.scheliga/trashpanda/badge.svg?branch=master)](https://coveralls.io/gitlab/david.scheliga/trashpanda?branch=master)
[![Build Status](https://travis-ci.com/david.scheliga/trashpanda.svg?branch=master)](https://travis-ci.com/david.scheliga/trashpanda)
[![PyPi](https://img.shields.io/pypi/v/trashpanda.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/trashpanda/)
[![Python Versions](https://img.shields.io/pypi/pyversions/trashpanda.svg?style=flat-square&label=PyPI)](https://https://pypi.org/project/trashpanda/)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Documentation Status](https://readthedocs.org/projects/trashpanda/badge/?version=latest)](https://trashpanda.readthedocs.io/en/latest/?badge=latest)

**trashpanda** is a collection of helper methods composing tasks around
pandas.DataFrames and Series. Either they are to specific (or I didn't found
them).

![trashpanda icon](https://trashpanda.readthedocs.io/en/latest/_images/trashpanda-icon.svg "A trash panda")

## Installing

Installing the latest release using pip is recommended.

````shell script
$ pip install trashpanda
````

## Usage

The api reference with all available methods can be found at 
https://trashpanda.readthedocs.io/en/latest/


## Authors

* **David Scheliga** 
    [@gitlab](https://gitlab.com/david.scheliga)
    [@Linkedin](https://www.linkedin.com/in/david-scheliga-576984171/)
    - Initial work
    - Maintainer

## License

This project is licensed under the GNU GENERAL PUBLIC LICENSE - see the
[LICENSE](https://gitlab.com/david.scheliga/dicthandling/blob/master/LICENSE) file for details

## Acknowledge

- [Code style: black](https://github.com/psf/black)
- [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
- [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)